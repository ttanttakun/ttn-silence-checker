# ttn-silence-checker
MIT License

Checks [ARAS's](http://aras.sourceforge.net/) (or another radio automation software) audio output and main mixer's audio output for silence and sends alert mails. If there is no sound coming from ARAS tries to kill the existing `aras-player` process (use [ttn-crown](https://gitlab.com/ttanttakun/ttn-crown) to restart ARAS).

You need two HTTP audio streams, we are using two `darkice` instances, routed with a `qjackctl` patch, streaming to an `Icecast2` server.

## Install
Install `sox`: `sudo apt-get install sox`.

Then `git clone` this repo and run `npm install` inside the folder
## Conf
copy `env_example` to `.env` and set your values

## Run
`node index.js`

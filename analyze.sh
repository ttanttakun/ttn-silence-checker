#!/bin/bash

function analyse {
  cat /tmp/$1* > /tmp/combined.$1
  AMPLITUDE=$(/usr/bin/sox /tmp/combined.$1 -n stat 2>&1 | awk '/RMS.+amplitude/ {print $NF}')
  SOUND=$(bc <<< "$AMPLITUDE>0.001")
  echo $SOUND
  # if [ $SOUND -eq 1 ]
  # then
  #   echo "yep"
  # else
  #   echo "nope"
  # fi
}

analyse $1

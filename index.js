require('dotenv').config();
var exec = require('child_process').exec;
var CronJob = require('cron').CronJob;
var nodemailer = require('nodemailer');

var transporter = nodemailer.createTransport(process.env.MAIL_TRANSPORT);

var arasMail = {
    from: process.env.MAIL_FROM,
    to: process.env.ARAS_MAIL_TO,
    subject: process.env.ARAS_MAIL_SUBJECT || '[Alerta] Aras fail',
    text: process.env.ARAS_MAIL_BODY || 'Aras fail',
    html: process.env.ARAS_MAIL_BODY || 'Aras fail'
};

var mixerMail = {
    from: process.env.MAIL_FROM,
    to: process.env.MIXER_MAIL_TO,
    subject: process.env.MIXER_MAIL_SUBJECT || '[Alerta] Soinurik ez!',
    text: process.env.MIXER_MAIL_BODY || 'Mahaiak ez du soinurik itzultzen, seguruna emisioaren kanala jetsita dagoela. Ahal duena gerturatu dadila, mila esker!',
    html: process.env.MIXER_MAIL_BODY || 'Mahaiak ez du soinurik itzultzen, seguruna emisioaren kanala jetsita dagoela. Ahal duena gerturatu dadila, mila esker!'
};

var mixerMailSended = false;

function sample(streamUrl, name) {
  exec('./sample.sh'+' ' + streamUrl + ' ' + name , function(error, stdout, stderr) { });
}

function analyse(name, callback) {
  exec('./analyze.sh'+' '+ name , function(error, stdout, stderr) {
    callback(parseInt(stdout));
  });
}

function clear(name) {
  exec('./clear.sh'+' '+ name , function(error, stdout, stderr) { });
}

function killAras() {
  exec('killall aras-player', function(error, stdout, stderr) { });
}

new CronJob('0 * * * * *', function() {
  sample(process.env.MIXER_STREAM, 'silencer-mixer');
  sample(process.env.ARAS_STREAM, 'silencer-aras');
}, null, true, 'America/Los_Angeles');

new CronJob('0 * */6 * * *', function() {
  // reset mixerMailSended
  mixerMailSended = false;
}, null, true, 'America/Los_Angeles');

new CronJob('0 */15 * * * *', function() {
  analyse('silencer-mixer', function(status){
    doSomeLogic('mixer', status);
    clear('silencer-mixer');
  });
  analyse('silencer-aras', function(status){
    doSomeLogic('aras', status);
    clear('silencer-aras');
  });

  var channels = {};
  function doSomeLogic(name, status){
    channels[name] = status;
    if(Object.keys(channels).length === 2) {
      if(channels.aras && !channels.mixer) {
        // console.log("MIXER IS DOWN");
        if(!mixerMailSended){
          transporter.sendMail(mixerMail, function(error, info){
            if(error){
              return console.log(error);
            }
            mixerMailSended = true;
            console.log('MIXER MAIL SENT: ' + info.response);
          });
        }
      } else if(!channels.aras) {
        // console.log("ARAS IS DOWN");
        killAras();
        transporter.sendMail(arasMail, function(error, info){
            if(error){
                return console.log(error);
            }
            console.log('ARAS MAIL SENT: ' + info.response);
        });
      } else {
        // console.log("EVERYTHING IS OK");
      }
    }
  }
}, null, true, 'America/Los_Angeles');

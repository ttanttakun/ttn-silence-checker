#!/bin/bash

function sample {
  timeout $1 wget $2 -O /tmp/$3-$(date +%M)
}

DELAY=$(( ( RANDOM % 50 )  + 1 ))
sleep $DELAY
# sample 10 http://10.107.2.50:8000/mixer.ogg mixer.ogg & sample 10 http://10.107.2.50:8000/aras.ogg aras.ogg
sample 10 $1 $2
